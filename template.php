<blockquote>

    <h3>
        <?php echo $meta_data->title; ?>
    </h3>

    <p>
        <?php echo $meta_data->description; ?>
    </p>

    <?php if ( 'hide' !== $atts['image'] ) : ?>

        <p>
            <img src="<?php echo $meta_data->image; ?>" alt="<?php echo $meta_data->title; ?>">
        </p>

    <?php endif; ?>

    <p>
        <a href="<?php echo $meta_data->url; ?>" class="more-link">
            <?php _e( 'Visit website', 'naidanud' ); ?>
        </a>
    </p>

</blockquote>