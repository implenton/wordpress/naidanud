<?php
/*
Plugin Name:       Naidanud
Plugin URI:        https://github.com/implenton/naidanud/
Description:       Rich URL embeds
Version:           1.1.0
Text Domain:       naidanud
Author:            implenton
Author URI:        https://implenton.com
GitHub Plugin URI: implenton/naidanud
License:           GPLv3
License URI:       https://www.gnu.org/licenses/gpl-3.0.txt
*/

namespace Naidanud;

if ( ! defined( 'WPINC' ) ) {
    die;
}

require 'vendor/autoload.php';

function shortcode( $atts ) {

    $atts = shortcode_atts( [
        'url'   => '',
        'image' => 'show',
    ], $atts );

    if ( empty( $atts['url'] ) ) {

        return '';

    }

    $template_file = apply_filters( 'naidanud_template_file', plugin_dir_path( __FILE__ ) . 'template.php' );
    $meta_data     = handle( $atts['url'] );

    ob_start();

    include( $template_file );

    return ob_get_clean();
}

add_shortcode( 'naidanud', __NAMESPACE__ . '\shortcode' );

function handle( $url ) {

    $transient_prefix = apply_filters( 'naidanud_transient_prefix', 'naidanud_' );
    $transient_name   = apply_filters( 'naidanud_transient_name', sanitize_title( $url ) );

    if ( false === ( $meta_data = get_transient( $transient_prefix . $transient_name ) ) ) {

        $meta_info = fetch_meta( $url );
        $meta_data = setup_meta_data( $meta_info );

        $transient_expiration = apply_filters( 'naidanud_transient_expiration', 7 * DAY_IN_SECONDS );

        set_transient( $transient_prefix . $transient_name, $meta_data, $transient_expiration );

    }

    return json_decode( $meta_data );

}

function fetch_meta( $url ) {

    $info = \Embed\Embed::create( $url );

    return apply_filters( 'naidanud_meta_info', $info, $url );
}

function setup_meta_data( $meta_info ) {

    $meta_data = [
        'title'       => $meta_info->providerName,
        'description' => $meta_info->description,
        'image'       => $meta_info->image,
        'url'         => $meta_info->url,
    ];

    return apply_filters( 'naidanud_data', json_encode( $meta_data ), $meta_info );

}