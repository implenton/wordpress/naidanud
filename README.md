# Naidanud

## Bird's-eye view

Once the plugin is installed you can use the `naidanud` shortcode.

Example: `[naidanud url="https://www.website.com]`

When you visit your post the plugin fetches the provider name, the description, the main image and the canonical url from the page. _Be aware, the first time you visit your post the loading time will be longer._

These informations are saved to a transient and passed to a partial file where HTML boilerplate resides.

The transients by default are prefixed with `naidanud_` followed by the URL's host. The expiration time is one week.

Example: `naidanud_www_website_com`.

_Note:_ Check out [Transient Manager](https://wordpress.org/plugins/transients-manager/) developed by [Pippin Williamson](https://pippinsplugins.com/).

> This is a developer's tool that provides a UI to manage your site's transients. You can view, search, edit, and delete transients at will. A toolbar option is also provided that allows you to suspend transient updates to help with testing and debugging.

## Embed library

This plugin uses the [Embed library](https://github.com/oscarotero/Embed) developed by [Oscar Otero](https://github.com/oscarotero).

> PHP library to get information from any web page (using oembed, opengraph, twitter-cards, scrapping the html, etc). It's compatible with any web service (youtube, vimeo, flickr, instagram, etc) and has adapters to some sites like (archive.org, github, facebook, etc).

Embed library's requirements:

- PHP 5.5+
- Curl library installed

## Installation

Since this plugin is not hosted by the WordPress' plugin repo, I recommend using [GitHub Updater](https://github.com/afragen/github-updater).

> This plugin was designed to simply update any GitHub hosted WordPress plugin or theme. Your plugin or theme must contain a header in the style.css header or in the plugin's header denoting the location on GitHub.

If you want to install it manually you can do that using the Admin Plugin Upload. There is an article on [WPBeginner](http://www.wpbeginner.com/beginners-guide/step-by-step-guide-to-install-a-wordpress-plugin-for-beginners/) explaining the process.

You can download the latest zippped release from here: https://github.com/implenton/naidanud/releases

## Demo

…

### Filters

- `naidanud_template_file`: to overwrite the default template file
- `naidanud_transient_prefix`: to change the transient prefix
- `naidanud_transient_expiration`: to alter the transient expiration name
- `naidanud_transient_name`: in case you want use different convention for the transient name
- `naidanud_meta_info`
- `naidanud_data`

#### If you prefer a different shortcode:

```php
add_shortcode( 'rich-url', '\Naidanud\shortcode' );
```

#### To use your own template file:

```php
add_filter( 'naidanud_template_file', 'naidanud_theme_template' );

function naidanud_theme_template() {

  return get_theme_file_path( '/template-parts/url-embed.php' );

}
```
